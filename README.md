# Welcome ==
Fastbucks is a simple, no-frills JIRA Add-On that automates invoicing your clients from JIRA projects.

Invoices are based on JIRA worklogs. 

Fastbucks connects to an online invoicing system such as Xero via OAuth, schedules the invoicing process and creates invoices per JIRA project.  The summary of time spent on each issue is invoiced as a separate item.

## Installation ==
Install via JIRA plugin management console.

## Setup ==
### Xero Client Setup ===

1. Register Fastbucks as a [[http://blog.xero.com/developer/api-overview/setup-an-application/#private-apps|private application in Xero]]. Keep a copy of the ASCII version of the private key used to generate the X.509 certificate
1. Goto to the Accounting section in the JIRA admin section of your project
1. Enter the 'Consumer Key' as provided by Xero
1. Enter the 'Consumer Secret' as provided by Xero
1. Copy and paste the private key contents into the private key field
1. Enter the Xero API Url as specified by Xero.

### Project Setup ===

1. Select Xero as the project client
1. Enter the hourly rate for this project. This value will be multiplied with the amount of time spent on issues.
1. Enter the Cron Expression Value. The Cron Expression value tells the Fastbucks scheduler when to send an invoice. Use the cron editor by clicking on the Cron Expression input field or enter a cron expression directly. When the time is up, Fastbucks will analyse the project's issue worklog, accumulate the time spent and send an itemized (per issue) invoice to Xero
1. Enter the Billing Contact. This must be the name of an existing Xero Contact to which the invoice will be sent to.

## More Clients? ==

Fastbucks has a generic and pluggable accounting system client interface. We will add client support for freshbooks and other systems in the upcoming releases.
Once the Fastbucks client API is stable it will be open-sourced to enable others to provide support for more accounting systems.


# The Nuts and Bolts ==
## Configuration ===
Each JIRA project administration page has an 'Accounting' section. Two things are configured and tested here.
### Accounting System Client ====
The Accounting System Client connects to the accounting system of your choice. Currently only Xero is supported - more are coming are on their way...
Depending on the type of connection this section allows to enter all necessary information for the client to connect to the accounting system. In the case of the Xero client the connection is based on 2-legged OAuth. A consumer key, a consumer secret and a private key are required to identify the client in Xero.
The correctness of the client parameters can be tested using the 'Test' link.
### Project Configuration ====
The parameters of this section are common to all projects. Here the hourly rate for work done on this project and the invoice schedule as a cron expression are entered. No need to grow grey hair - the built-in JIRA cron editor helps to schedule invoices.
Finally the contact name of the client that should receive the invoice needs to be provided.
The client contact should pre-exist in the accounting system, its existence can be verified by clicking 'Test' in this section.
   
## Scheduler ===
The scheduler runs according to the cron expression set in the project configuration.
Here is how it works:

1. The time when the configuration is saved or the license is entered marks the starting point for the scheduler
1. At the next cron trigger time all worklog entries of all issues in the project that have been updated after the starting time are accumulated and itemized in the invoice. Each worklog entry that is accounted for in the new invoice is tagged with the invoice number. This is a safety net to prevent duplicated invoicing of work done on the project.
1. Finally the next trigger event is calculated and scheduled. The new starting point is set to the previous trigger time.


